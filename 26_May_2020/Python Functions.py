#!/usr/bin/env python
# coding: utf-8

# ### Why we use Functions?
# - Reduce Complexity
# - Moduler
# - Resusability
# 
# ### Types of functions?
# - Built In
# - User Defined
# 
# - Parametrized Function
#     
#     def add(x,y):
#         return x+y
#     
# - Non-parametrized Function
#     
#     def abc():
#             return "Hii"
#         
# ### Parts of a function
# def abcd():  #Definition
#     
#     print("Hii") #Body
#     
# abcd() #calling

# In[5]:


def abcd():
    print("Hello Everyone")
    print("Last statement")
abcd()


# In[8]:


def add(x,y,z):
    print(x+y+z)

add(10,87,7)


# ### return vs print

# In[21]:


def add(x,y):
    print(x+y)
    print("End of function")
    return 1
add(10,29)*10


# In[22]:


def hello():
    return 10
    
hello()+10
hello()*10


# In[29]:


def mul(x,y,z):
    return x*y*z
    print("HelloEveryone")
    
print(mul(1,2,3)*10)


# In[36]:


### Convert Snake case to camel case
# my_name_is_this = "aman" #snakecase
# myNameIsThis #camelcas

name = input("Enter name : ")
print(name.replace(" ","_"))


# In[39]:


name = input("Enter name in Snake Case: ")
name.replace("_"," ").title().replace(" ","")

