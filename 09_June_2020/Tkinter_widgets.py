#!/usr/bin/env python
# coding: utf-8

# ### Geometry managers

# In[126]:


import tkinter
from tkinter import messagebox

win = tkinter.Tk()
win.title("Login Form")
win.resizable(0,0)
win.config(bg="blue")

def loginprocess():
    un = en1.get()
    pwd = en2.get()
    if un==pwd:
        root = tkinter.Tk()
        root.title("Dashboard | Login Success")
        root.geometry("200x200")
        label = tkinter.Label(root,text="Welcome "+un,fg="green",font=("arial",20,"italic"))
        label.pack()
        root.mainloop()
    else:
        messagebox.showerror("Invalid Details","Incorrect Username or Password")
## Adding widget to root window
label = tkinter.Label(win,text="LOGIN FORM")
label.pack()

lbf = tkinter.LabelFrame(win,text="LOGIN HERE",bg="white",width=300)
lbf.pack(side="right",ipady=5)

lbf1 = tkinter.Frame(win,bg="pink",height=100,width=300)
lbf1.pack(side="left")


### Adding widgets to label frame
lb1 = tkinter.Label(lbf,text="Enter Username")
lb2 = tkinter.Label(lbf,text="Enter Password")

en1 = tkinter.Entry(lbf)
en2 = tkinter.Entry(lbf,show="*")

btn = tkinter.Button(lbf,text="Click to Continue",relief="flat",width="30",bg="green",fg="white",command=loginprocess)

lb1.grid(row=0,column=0)
en1.grid(row=0,column=1)

lb2.grid(row=1,column=0)
en2.grid(row=1,column=1)
btn.grid(row=2,column=0,columnspan=2)

# lb1.place(x=100,y=100)
# en1.place(x=100,y=120)
# lb2.place(x=100,y=150)
# en2.place(x=100,y=170)
# btn.place(x=100,y=200)

# lb1.pack()
# en1.pack()
# lb2.pack()
# en2.pack()
# btn.pack(pady=10)

win.geometry("500x500")
win.mainloop()


# ### Listbox

# In[164]:


from tkinter import *
from tkinter import messagebox

root = Tk()
root.title("TKINTER LISTBOX")

def getval():
    v = lb.curselection()
    messagebox.showinfo("VALUE","Selected Value: {}".format(v))

lb = Listbox(root)

lb.insert(0,"Hii")

# lb.insert(1,"HTML")
for i in range(1,100):
    lb.insert(i,"Number is: {}".format(i))

    
lb.insert(END,"PYTHON CODE")
lb.pack()

btn = Button(root,text="Get Value",bg="green",fg="white",command=getval)
btn.pack()
root.geometry("500x200")
root.mainloop()

