#!/usr/bin/env python
# coding: utf-8

# In[1]:


import requests


# In[18]:


con = requests.get("https://restcountries.eu/rest/v2/all").json()
print(con)


# In[22]:


name = input("Enter Country Name").title()
data = ""
for i in con:
    if i["name"]==name:
        data += "<div style='text-align:center;height:400px;width:30%;margin:auto;box-shadow:0px 0px 10px red;padding:10px;'>"
        data +="<img src='{}' style='height:200px;width:100%;'>".format(i["flag"])
        data+="<h1>{}</h1>".format(i["name"])
        data+="<p><em>{}</em></p>".format(i["population"])
        data+="<p><em>{}</em></p>".format(i["borders"])
        data+="</div>"

file = name+".html"
with open(file,"w") as f:
    f.write(data)
    
print("file written successfully")

