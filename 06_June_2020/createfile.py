import os

try:
    f = open("abcd.txt","x")
    f.write("Hello EveryOne!!!")
    f.close()
    print("File created successfully!!!")

except FileExistsError:
    os.remove("abcd.txt")
    print("File With this name already exists!!!")
    print("File removed successfully!!!")