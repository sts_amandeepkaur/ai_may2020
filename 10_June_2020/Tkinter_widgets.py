#!/usr/bin/env python
# coding: utf-8

# In[34]:


from tkinter import *
from tkinter import messagebox


root = Tk()
root.title("Tkinter CheckBox")

def get_val():
    ab=var1.get()
    messagebox.showinfo("Message","Selected value: {}".format(ab))

def abcd():
    check.toggle()
    
var1 = StringVar()

check = Checkbutton(root,text="Python",variable=var1,onvalue="Python",offvalue="No value",pady=10)
check.deselect()
check.pack()

btn = Button(root,text="Get Value",bg="green",fg="white",command=get_val)
btn.pack()

btn1 = Button(root,text="Select / unselect",bg="orange",fg="white",command=abcd)
btn1.pack()

root.geometry("500x500")
root.mainloop()


# ### Tkinter Menu

# In[17]:


from tkinter import *

win = Tk()
var = IntVar()
def welcome():
    root = Tk()
    root.title("Welcome")
    
    lbl = Label(root,text="WELCOME TO NEW WINDOW", fg="green",font=("Arial",20))
    lbl.pack()
    root.geometry("500x500")
    root.mainloop()
    
def get_value():
    val=var.get()
    lbl.config(text="Selected Value: {}".format(val))
    
def register():
    
    r1 = Radiobutton(win,text="Male",variable=var,value=0,command=get_value)
    r1.pack()
    
    r2 = Radiobutton(win,text="Female",variable=var,value=1,command=get_value)
    r2.pack()

    r3 = Radiobutton(win,text="Others",variable=var,value=2,command=get_value)
    r3.pack()
    

menubar = Menu(win)

home = Menu(menubar,tearoff=0)
home.add_command(label="New",command=welcome)
home.add_command(label="Edit",command=register)
home.add_command(label="Exit...",command=win.destroy)

edit =Menu(menubar,tearoff=0)
edit.add_command(label="Cut")
edit.add_command(label="Copy")
edit.add_separator()
edit.add_command(label="Paste")


menubar.add_cascade(label="File",menu=home)
menubar.add_cascade(label="Edit",menu=edit)
lbl = Label(win,fg="red",font=("arial",20))
lbl.pack()
win.config(menu=menubar)
win.geometry("200x200")
win.mainloop()

