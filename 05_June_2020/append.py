#Open File
ab = open("tables.html","a")

#Write File
num = int(input("Enter Number: "))
ab.write("<h1 style='text-align:center;text-shadow:0px 0px 20px red;'> Here is table of :{} </h1>".format(num))
for i in range(1,11):
    ab.write("<h3 style='color:red;'>{} * {} = <ins style='color:green;'>{}</ins></h3>".format(num,i,num*i))
ab.write("<hr>")

#close file
ab.close()

# ab.write("ray5h6erdtxvljhq35slizvhwa;34tawrhoirej;oi") #We cant do I/O operation on closed file.