#!/usr/bin/env python
# coding: utf-8

# ### What construcors and destructors
# 
# Constructors are special type of Python Functions which automatically calls on object (instance) creation.
# We use constructors to initialize values
# 
# In other hand, destructors are special type of Python Functions which automatically calls on object (instance) creation
# but used to destroy objects. In simple words we can say it frees resources occupied by objects.

# In[9]:


class MyClass:
    
    def fun(self):
        print("It is a member function")
        

obj = MyClass()
obj.fun()


# ### Now using constructor...

# In[30]:


class MyClass:
    #Create Constructor
    def __init__(self):
        print("Constructor Function")
        
obj= MyClass()


# In[29]:


class Circle:
    pi=3.14
    
    def set_radius(self,rad):
        self.r = rad
        print("Radius Set")
        
    def Area(self):
        print("\nRadius: {}".format(self.r))
        print("Area: {}\n".format(self.pi * self.r **2 ))
        
c1 = Circle()
c2 = Circle()
c3 = Circle()

c1.set_radius(3)
c2.set_radius(7)
c3.set_radius(10)

c1.Area()
c2.Area()
c3.Area()


# Now using constructor...

# In[26]:


class Circle:
    pi=3.14
    def __init__(self,rad):
        self.r = rad
        print("Value Initialized")
        
    def Area(self):
        print("Radius:{}".format(self.r))
        print("Area: {} * {} **2 = {}".format(self.pi,self.r, self.pi*self.r**2))
        
    
        
c1 = Circle(20)
c2 = Circle(15)
c3 = Circle(10)

c1.Area()
c3.Area()

