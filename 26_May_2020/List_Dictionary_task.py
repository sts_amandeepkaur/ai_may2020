#!/usr/bin/env python
# coding: utf-8

# In[ ]:


options = """
    Press 1: To add User
    Press 2: To view Users
    Press 3: To Remove User
    Press 4: To Exit
"""
print(options)
users =[]
while True:
    
    ch = input("Enter Your Choice: ")
    if ch=="1":
        name = input("Enter Your Name: ")
        roll = input("Enter Roll Number: ")
        usr = {
            "name":name,"roll_no":roll
        }
        users.append(usr)
        print(name,"added successfully!\n")
        
    elif ch=="2":
        print("Total: ",len(users))
        
        for i in users:
            print("Name: {} Roll No.: {}".format(i["name"],i["roll_no"]))
            
    elif ch=="3":
        r = input("Enter Roll Number to delete: ")
        found=False
        for i in users:
            if i["roll_no"]==r:
                found=True
                users.remove(i)
                print(i["name"],"removed Successfully!!")
                
        if found==False:
            print("No user exists with this roll Number: ")
            
                
    elif ch=="4":
        break
    else:
        print("Invalid Choice")
        


# In[ ]:




