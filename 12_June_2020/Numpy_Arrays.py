#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[2]:


arr = np.array([
    [1,2,4],
    [100,200,400],
    [100,456,75]
])


# In[3]:


arr.shape


# In[5]:


print(arr.dtype)
arr.size


# ### Indexing / Slicing

# In[9]:


arr[-1]


# In[13]:


arr = np.array([
    [1,2,4],
    [100,200,400],
    [100,456,75]
])
print(arr[0,2])


# In[16]:


arr[:,-1]


# In[21]:


arr[:2,1:]


# In[30]:


arr[0,1]


# In[32]:


arr[::2,::2]


# In[74]:


myarr = np.array([
    [1,2,3],
    [4,5,6],
    [7,8,9],
    [10,11,13],
])


# In[69]:


ls = [10,30,40,50]
ls[2:]


# In[78]:


myarr[::-2,::2]


# ### where() - function compare two arrays & returns indexes according to condition

# In[141]:


a = np.array([5,6,7,8])
b = np.array([3,6,7,9])


np.where(a !=b )
np.where(a == b)
np.where( a>b )
np.where(a<b)

a*b


# ### concaenate()

# In[92]:


x=np.array([
    [1,2],
    [3,4]
])

y = np.array([
    [5,6],
    [1,4]
   
])

np.concatenate((x,y),axis=0)


# In[94]:


np.concatenate((x,y),axis=1)


# ### hstack() vstack()

# In[95]:


x


# In[96]:


y


# In[98]:


np.hstack((x,y))  #To merge arrays horizontally (column)


# In[99]:


np.vstack((x,y)) 


# ### unique()

# In[102]:


rol_no =np.array([2,10,20,30,2,2,1,2,30,10])
rol_no


# In[103]:


np.unique(rol_no)


# ### bincount() & argmax()

# In[113]:


arr = np.array([2,4,5,1,10,1,2,1,10,7,7,7,7])
np.bincount(arr).argmax()


# ### Changing shape of numpy array

# In[123]:


ab = np.array([0,1,2,3,4,5,6,7,8])
ab


# In[117]:


ab.shape, ab.ndim


# In[125]:


cd=ab.reshape(3,3)
cd


# In[126]:


cd.ndim


# In[129]:


ab.reshape(-1,1)


# In[130]:


ab.reshape(1,-1)


# In[133]:


xy = np.array([0,1,2,3,4,5,6,7,8,9])


# In[134]:


xy.reshape(-1,2)


# In[135]:


xy.reshape(2,-1)


# ### matmul & dot()

# In[136]:


x = np.array([
    [1,2],
    [2,4]
])
y = np.array([
    [1,3],
    [2,6]
])

# 1*1+2*2   1*3+2*6


# In[137]:


np.matmul(x,y)


# In[138]:


np.dot(x,y)


# In[142]:


x*y


# ### Generate Random Arrays

# In[163]:


np.random.randint(1,10)


# In[210]:


np.random.seed(0)
np.random.randint(1,10,(2,2))


# In[228]:


np.random.rand(2,3)


# In[237]:


np.linspace(0,1,5)


# In[235]:


np.linspace(0,1,50)

