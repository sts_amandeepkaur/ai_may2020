from chatterbot import ChatBot
import tkinter as tk
from tkinter import messagebox
from chatterbot.trainers import ChatterBotCorpusTrainer
import speech_recognition as sr
import requests
r = sr.Recognizer()   
import pyttsx3
try:
    import ttk as ttk
    import ScrolledText
except ImportError:
    import tkinter.ttk as ttk
    import tkinter.scrolledtext as ScrolledText
import time


class TkinterGUIExample(tk.Tk):

    def __init__(self, *args, **kwargs):
        """
        Create & set window variables.
        """
        tk.Tk.__init__(self, *args, **kwargs)

        self.chatbot = ChatBot("Chatterbot",storage_adapter = "chatterbot.storage.SQLStorageAdapter",read_only='True')
        # self.trainer = ChatterBotCorpusTrainer(self.chatbot)
        # self.trainer.train("chatterbot.corpus.english")
        # Gets the requested values of the height and widht.
        windowWidth = self.winfo_reqwidth()
        windowHeight = self.winfo_reqheight()
        
        # Gets both half the screen width/height and window width/height
        positionRight = int(self.winfo_screenwidth()/3 - windowWidth/2)
        positionDown = int(self.winfo_screenheight()/3 - windowHeight/2)
        
        # Positions the window in the center of the page.
        self.geometry("+{}+{}".format(positionRight, positionDown))
        self.title("Chatterbot")
        self.configure(background='red')
        self.resizable(height = 0, width = 0)
        self.initialize()

    def initialize(self):
        """
        Set window layout.
        """
        self.grid()
        self.respond = tk.Button(self, text='Send', command=self.get_response ,fg='white',bg='blue')
        self.respond.grid(column=1, row=3, sticky='nesw', padx=3, pady=3)
        self.respond1 = tk.Button(self, text='Speak', command=self.speaking,fg='white',bg='blue')
        self.respond1.grid(column=2, row=3, sticky='nesw', padx=3, pady=3)
        self.respond2 = tk.Button(self, text='Weather', command=self.weather,fg='white',bg='blue')
        self.respond2.grid(column=3, row=3, sticky='nesw', padx=3, pady=3)
        self.usr_input = tk.Entry(self, state='normal',font=("Courier", 15))
        self.usr_input.grid(column=0, row=3, sticky='nesw', padx=3, pady=3,ipady=10)
        self.conversation_lbl = tk.Label(self, text='CHATBOT',font=("Courier", 30) , bg='red',fg='white')
        self.conversation_lbl.grid(columnspan=4, row=0, padx=5, pady=5)
        self.conversation = ScrolledText.ScrolledText(self, state='disabled')
        self.conversation.grid(column=0, row=2, columnspan=4, padx=3, pady=3 , ipadx=10,ipady=10)
    def get_response(self):
        """
        Get a response from the chatbot and display it.
        """
        user_input = self.usr_input.get()
        self.usr_input.delete(0, tk.END)
        response = self.chatbot.get_response(user_input)
        label1 = tk.Label(self.conversation, text=user_input, background='blue', justify='left', fg='white' ,padx=10, pady=5,border='3px solid black')
        label2 = tk.Label(self.conversation, text=response, background='green', justify='right', padx=10, pady=5,border='3px solid black',fg='white')
        self.conversation.tag_configure('tag-left', justify='left')
        self.conversation.tag_configure('tag-right', justify='right')
        self.conversation['state'] = 'normal'
        self.conversation.insert(
            tk.END, "\n" 
        )
        self.conversation.window_create('end', window=label1)
        self.conversation.insert('end', '\n ', 'tag-right') 
        self.conversation.window_create('end', window=label2)
        self.conversation['state'] = 'disabled'
        engine = pyttsx3.init() 
        engine.setProperty('rate', 150) 
        engine.setProperty('volume', 0.7) 
        engine.say(response)
        engine.runAndWait() 
        time.sleep(0.5)
    def speaking(self):
        with sr.Microphone() as source:
            r.adjust_for_ambient_noise(source, duration=0.2) 
            audio = r.record(source,duration=3)
        try:
                    user_input= r.recognize_google(audio)
                    print(user_input)
                    self.resp(user_input)
              
        except:
                tk.messagebox.showwarning(title='Audio Not Recognised', message='Sorry We Are Unable To Recognise That.')
       
    def resp(self,ui):
                    user_input=ui
                    response = self.chatbot.get_response(user_input)
                   
                    label1 = tk.Label(self.conversation, text=user_input, background='blue', justify='left', fg='white' ,padx=10, pady=5,border='3px solid black')
                    label2 = tk.Label(self.conversation, text=response, background='green', justify='right', padx=10, pady=5,border='3px solid black',fg='white')
                    self.conversation.tag_configure('tag-left', justify='left')
                    self.conversation.tag_configure('tag-right', justify='right')
                    self.conversation['state'] = 'normal'
                    self.conversation.insert(
                        tk.END, "\n" 
                    )
                    self.conversation.window_create('end', window=label1)
                    self.conversation.insert('end', '\n ', 'tag-right') 
                    self.conversation.window_create('end', window=label2)
                    self.conversation['state'] = 'disabled'
                    time.sleep(0.5)
                    engine = pyttsx3.init() 
                    engine.setProperty('rate', 150) 
                    engine.setProperty('volume', 0.7) 
                    engine.say(response)
                    engine.runAndWait() 
                    
    def SpeakText(self,city):
                    URL = "https://api.openweathermap.org/data/2.5/weather?q={}&appid=e79a7d07c4e3bf54dd170836c3e88496&units=metric".format(city)
                    data = requests.get(URL).json()
                    weather='weather of {}'.format(city)
                    weather2="{} ({})".format(data["weather"][0]["main"],data["weather"][0]["description"]+'\n'+"Temprature: {}°Celcius".format(data["main"]["temp"]))
                    if data["cod"]==200:
                        label1 = tk.Label(self.conversation, text=weather, background='blue', justify='left', fg='white' ,padx=10, pady=5,border='3px solid black')
                        label2 = tk.Label(self.conversation, text=weather2, background='green', justify='right', padx=10, pady=5,border='3px solid black',fg='white')
                        self.conversation.tag_configure('tag-right', justify='right')
                        self.conversation.tag_configure('tag-left', justify='left')
                        self.conversation['state'] = 'normal'
                        self.conversation.insert(
                            tk.END, "\n" 
                        )
                        self.conversation.window_create('end', window=label1)
                        self.conversation.insert('end', '\n ', 'tag-right') 
                        self.conversation.window_create('end', window=label2)
                        self.conversation['state'] = 'disabled'
                        time.sleep(0.5)
                        engine = pyttsx3.init() 
                        engine.say("{} ({})".format(data["weather"][0]["main"],data["weather"][0]["description"]))
                        engine.say("Temprature: {}°Celcius".format(data["main"]["temp"]))
                        engine.runAndWait()   
                    else:
                        engine = pyttsx3.init() 
                        engine.setProperty('rate', 150) 
                        engine.setProperty('volume', 0.7) 
                        engine.say("{} ".format(data["message"]))  
                        engine.runAndWait()   
    def weather(self):
                    with sr.Microphone() as source:  
                        r.adjust_for_ambient_noise(source, duration=0.2) 
                        audio = r.record(source,duration=5)
                        try:
                                text = r.recognize_google(audio)
                                print(text)
                                self.SpeakText(text)
                        except:
                            tk.messagebox.showwarning(title='Audio Not Recognised', message='Sorry We Are Unable To Recognise That.')     
gui_example = TkinterGUIExample()
gui_example.mainloop()
