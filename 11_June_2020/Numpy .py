#!/usr/bin/env python
# coding: utf-8

# Numpy is a python library for working with n-dimensional arrays

# In[1]:


### Numpy installation

get_ipython().system('pip install numpy')


# ### Create An array from list

# In[2]:


import numpy as np


# In[3]:


ls = [10,30,40,9]
print(type(ls))


# In[5]:


arr = np.array(ls)
print(type(arr))


# In[6]:


arr


# In[7]:


print(arr[0])


# In[8]:


print(arr[1:])


# ### Check Number of dimensions

# In[9]:


arr.ndim


# In[14]:


ab = np.array([
    [10,20,30],
    [22,5,7],
    [20,4,6]
])


# In[15]:


ab.ndim #No. of dimensions


# Scaler = 10
# 
# Vector = [10,30,50,60]
# 
# Matrix = [[10,20],[10,30]]

# In[16]:


print(ab.shape) #Returns no. of tupe(rows,columns) in array
print(arr.shape)


# In[17]:


ab.dtype #Data Type of array elements


# In[18]:


ab.size #Number of total elements

