#!/usr/bin/env python
# coding: utf-8

# In[ ]:





# In[66]:


import requests

nm = input("Enter City Name: ")
API_LINK = "https://api.openweathermap.org/data/2.5/weather?q="+nm+"&appid=e79a7d07c4e3bf54dd170836c3e88496&units=metric"

data = requests.get(API_LINK).json()

if data["cod"]=="404":
    print("City Not Found!!!")
else:
    name=data["name"]
    country = data["sys"]["country"]
    temp=data["main"]["temp"]
    main = data["weather"][0]["main"]
    desc = data["weather"][0]["description"]
    icon= data["weather"][0]["icon"]

    print("{} ({})".format(name,country))
    print("Temperature: {}°C".format(temp))
    print("{} ({})".format(main,desc))
    
    filename = name+".html"
    
    rs = "<meta chaeset='utf-8'><div style='width:30%;margin:auto;box-shadow:0px 0px 10px gray;padding:1%;text-align:center;'>"
    rs += "<img src='http://openweathermap.org/img/wn/"+icon+"@2x.png'>"
    rs += "<h1>{} ({})</h1>".format(nm,country)
    rs += "<p>Temperature: {}&deg;C</p>".format(temp)
    rs += "<em>{} ({})</em>".format(main,desc)
    rs+= "</div>"
    
    
    with open(filename,"w") as file:
        file.write(rs)
    print(filename,"written Successfully!!!")

