#!/usr/bin/env python
# coding: utf-8

# Tkinter is the standard GUI library for Python. It is used to create Desktop applications in Python.

# ### Creating Window

# In[24]:


import tkinter

main = tkinter.Tk()
main.title("TK Window")


main.geometry("500x200")
main.mainloop()


# ### Adding Widgets

# In[65]:


import tkinter as tk

win = tk.Tk()
win.resizable(0,0)
win.title("MY FIRST WINDOW")

label = tk.Label(win, text="Welcome To My App",fg="red",bg="yellow",font=("arial",29,"bold"))
label.pack(ipadx=100,ipady=50,fill="x")


nm = tk.Label(win,text="Enter Your Name")
nm.pack()

entry = tkinter.Entry(win)
entry.pack(pady=20,fill="x",padx=20,ipady=10)

btn = tkinter.Button(win,text="Click Me",bg="green",fg="white",font=("arial",10))
btn.pack(ipady=10,ipadx=30,fill="x",padx=20,pady=10)

win.mainloop()


# ### Widget Binding with Functions

# In[85]:


import tkinter as tk
from tkinter import messagebox

def abcd():
    val=entry.get()
    if val=="":
        messagebox.showerror("Empty Value for name","Username cannot be empty!!!")
        
#     elif val.isalnum():
#         messagebox.showwarning("WARNING!!!","Name must contain letters only!!!")
        
    else:
        lb.config(text="Welcome {} !!!".format(val),bg="pink")
        messagebox.showinfo("Login Success","Welcome {} !!!".format(val))
        


win = tk.Tk()
win.resizable(0,0)
win.title("MY FIRST WINDOW")

label = tk.Label(win, text="Welcome To My App",fg="red",bg="yellow",font=("arial",29,"bold"))
label.pack(ipadx=100,ipady=50,fill="x")


nm = tk.Label(win,text="Enter Your Name")
nm.pack()

entry = tkinter.Entry(win,show="*")
entry.pack(pady=20,fill="x",padx=20,ipady=10)

lb = tk.Label(win, fg="green",font=("arial",20))
lb.pack()


btn = tkinter.Button(win,text="Click Me",bg="green",fg="white",font=("arial",10),command=abcd)
btn.pack(ipady=10,ipadx=30,fill="x",padx=20,pady=10)

win.mainloop()

