#!/usr/bin/env python
# coding: utf-8

# 
# ### Class and Objects

# <img src='c.jpg'>

# In[10]:


#Creating Class
class Dog:
    breed=None
    size=None
    
    def myfun(self):
        print("It is a member function")

#Creating Object
dog1 = Dog()
#Change Data Members
dog1.breed = "lebra"
#Access data members
print(dog1.breed)
#Access member function
dog1.myfun()


# ### Let's take an another example...

# In[19]:



class Circle:
    pi=3.14
    
    def greet(self):
        print("Hello Everyone!!!")
        
    def Area(self,rad):
        print("Radius={}".format(rad))
        print("Result={}".format(self.pi*rad**2))
        
c1 = Circle()
c2 = Circle()

print(c1.pi)
c1.greet()

c1.Area(10)
c2.Area(100)


# ### Access attributes inside the class

# In[43]:


class Library:
    clz, branch = "XYZ Engg. College","CSE"
    
    def details(self,name,rno=None):
        self.n = name
        self.r=rno
        print(name,"Registred Successfully")
        
    def issue_book(self,bn,isdt,rtdt):
        print("\nCOLLEGE:{} \nBRANCH:{}".format(self.clz,self.branch))
        print(self.n,"has issued a book!!!\nRoll No: ",self.r)
        print("Book Name: {} Issue Date:{} Return Date:{}\n".format(bn,isdt,rtdt))
        
s1 = Library()
s2 = Library()
s3 = Library()
s4 = Library()

s1.details("Aman",2001)
s2.details("ferb",2002)
s3.details("jack",2003)
s4.details("Ruby",2004)

s3.issue_book("Python Programming","01-June-2020","15-June-2020")
s2.issue_book("Computer Graphics","31-May-2020","14-June-2020")

